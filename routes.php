<?php
use Quivi\Event\Models\Service;
use Quivi\Event\Models\Event;

use Illuminate\Support\Facades\Input;

App::before(function($request) {
    Route::post('/api/test', function (\Request $request) {
        return response()->json(('The test was successful'));
    })->middleware('\Tymon\JWTAuth\Middleware\GetUserFromToken');

    Route::group(['prefix' => 'api/events'], function () {
        Route::group(['prefix' => 'orders'], function () {

            Route::any('/calculatePrice', function (\Request $request) {

                $order = Input::post('order');
                $order['accomodations'] = NULL;
                $pax = $order['people'];
                $days = (strtotime($order['date_to']) -  strtotime($order['date_from']))/86400;
                $order['price'] = 0;
                $order['soldout'] = false;

                foreach ($order['bundle'] as $key_bundle => &$order_bundles) {                    
                    if (!in_array($key_bundle, ['bundle_accomodations','bundle_events','bundle_services','bundle_extras'])){
                        continue;
                    }

                    $key_bundle = substr($key_bundle, 0, -1);

                    foreach ($order_bundles as &$bundle_service){
                        $service = Service::find($bundle_service[$key_bundle]['id']);
                        $available = false;
                    
                        if ($service->isAvailable($pax)){
                            $order['services'][$service->id]['price'] = $service->calculatePrice($pax, $days);
                            $order['price'] += $order['services'][$service->id]['price'];
                            $available = true;   
                        }
                        
                        if (!empty($bundle_service[$key_bundle]['upgrades'])) {
                            
                            foreach ($bundle_service[$key_bundle]['upgrades'] as $key_upgrade => $upgrade){
                                if (!empty($upgrade['service_upgrade']['id'])) {
                                    $service = Service::find($upgrade['service_upgrade']['id']);
                                    
                                    if (!$service->isAvailable($pax)){
                                        // service soldout
                                        unset($bundle_service[$key_bundle]['upgrades'][$key_upgrade]);
                                        continue;
                                    }
                                    $order['services'][$service->id]['price'] = $service->calculatePrice($pax, $days);

                                    if (!$available) {
                                        // service fallback upgrade
                                        $order['price'] += $order['services'][$service->id]['price'];
                                        unset($bundle_service[$key_bundle]['upgrades'][$key_upgrade]);
                                        $available = true;

                                        foreach ($bundle_service[$key_bundle] as $k => $v){
                                            // override selected service
                                            if ($k != 'upgrades'){
                                                $bundle_service[$key_bundle][$k] = $service->$k;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (!$available) {
                            $order['soldout'] = 1;
                        }
                    }
                }


                return response()->json([

                    "order" => $order,
                    "price" => $order['price']/100,
                    "total" => $order['price']*$pax/100

                ]);

            });

            Route::any('/testPrice', function (\Request $request) {
                $event = Event::find(6);

                return response()->json([

                    "bundle" => $event->base_price,

                ]);

            });

            Route::post('/sendMessage', function (\Request $request) {

                // FARE QUA LA LOGICA DI INVIO EMAIL

                $vars = [
                    "form" => Input::get('form')
                ];

                Mail::send('quivi.event::mail.message', $vars, function($message) {

                    $message->to('info@bestitalianevents.com', 'Best Italian Events');
                    $message->subject('Nuova richiesta informazioni');

                });


                return response()->json([

                    "status" => "sent",

                    "form"  =>  Input::get('form')

                ]);

            });


        });

    });
});