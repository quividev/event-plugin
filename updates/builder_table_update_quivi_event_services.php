<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviEventServices extends Migration
{
    public function up()
    {
        Schema::table('quivi_event_services', function($table)
        {
            $table->text('descr')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('quivi_event_services', function($table)
        {
            $table->string('descr', 191)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}