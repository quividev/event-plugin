<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviEventServices2 extends Migration
{
    public function up()
    {
        Schema::table('quivi_event_services', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('quivi_event_services', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}
