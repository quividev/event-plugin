<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviEventEvents3 extends Migration
{
    public function up()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->text('video_credits')->nullable()->after('video_mp4');
        });
    }
    
    public function down()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->dropColumn('video_credits');
        });
    }
}
