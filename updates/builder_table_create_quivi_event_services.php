<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviEventServices extends Migration
{
    public function up()
    {
        Schema::create('quivi_event_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('event_id');
            $table->integer('product_id');
            $table->string('name')->nullable();
            $table->string('descr')->nullable();
            $table->integer('pax')->default(0);
            $table->boolean('is_daily')->default(false);
            $table->integer('stock')->default(0);
            $table->integer('sold')->default(0);
            $table->integer('price')->default(0);
            $table->integer('cost')->default(0);
            $table->integer('currency_id')->nullable();
            $table->datetime('date_from')->nullable();
            $table->datetime('date_to')->nullable();
            $table->text('upgrades')->nullable();
            $table->text('options')->nullable();
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_event_services');
    }
}
