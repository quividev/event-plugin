<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQuiviEventEvents extends Migration
{
    public function up()
    {
        Schema::create('quivi_event_events', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('event_id')->nullable();
            $table->string('name', 255);
            $table->string('slug', 255);
            $table->string('subtitle', 255)->nullable();
            $table->text('descr')->nullable();
            $table->string('header_class', 255)->nullable();
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->date('date_min')->nullable();
            $table->date('date_max')->nullable();
            $table->integer('stock')->default(0);
            $table->integer('sold')->default(0);
            $table->string('photo', 255)->nullable();
            $table->text('bundles')->nullable();
            $table->text('upsells')->nullable();
            $table->text('content')->nullable();
            $table->text('faqs')->nullable();
            $table->string('meta_descr', 4096)->nullable();
            $table->string('meta_keywords', 4096)->nullable();
            $table->string('og_image', 255)->nullable();
            $table->boolean('is_enabled')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('quivi_event_events');
        Schema::dropIfExists('quivi_event_shows');
        Schema::dropIfExists('quivi_event_dates');
        Schema::dropIfExists('quivi_event_dates_bundles');
        Schema::dropIfExists('quivi_event_events_products');
        Schema::dropIfExists('quivi_event_events_bundles');
        Schema::dropIfExists('quivi_event_shows_products');
    }
}
