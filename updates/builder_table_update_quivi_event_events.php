<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviEventEvents extends Migration
{
    public function up()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->boolean('is_available')->default(1)->after('is_enabled');
        });
    }

    public function down()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->dropColumn('is_available');
        });
    }
}