<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviEventEvents4 extends Migration
{
    public function up()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->string('cover_vertical', 255)->nullable()->after('photo');
            $table->string('cover_squared', 255)->nullable()->after('photo');
            $table->renameColumn('photo', 'cover_horizontal');
        });
    }
    
    public function down()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->dropColumn('cover_vertical');
            $table->dropColumn('cover_squared');
            $table->renameColumn('cover_horizontal', 'photo');
        });
    }
}
