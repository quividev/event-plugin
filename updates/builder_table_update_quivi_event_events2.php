<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviEventEvents2 extends Migration
{
    public function up()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->boolean('is_coming_soon')->default(0)->after('is_available');;
        });
    }

    public function down()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->dropColumn('is_coming_soon');
        });
    }
}