<?php namespace Quivi\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateQuiviEventEvents2 extends Migration
{
    public function up()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->string('video_mp4')->nullable()->after('photo');
            $table->string('video_webm')->nullable()->after('photo');
        });
    }
    
    public function down()
    {
        Schema::table('quivi_event_events', function($table)
        {
            $table->dropColumn('video_mp4');
            $table->dropColumn('video_webm');
        });
    }
}
