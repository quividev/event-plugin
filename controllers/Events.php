<?php namespace Quivi\Event\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

use Quivi\Event\Models\Event;

class Events extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend.Behaviors.RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';


    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Quivi.Best', 'best-main-menu-item', 'events');
    }



    public function onDuplicate() {

        $checked_items_ids = input('checked');

        foreach ($checked_items_ids as $id) {

            $original = \Quivi\Event\Models\Event::where("id", $id)->first();
            $relations = [];

            $clone = $original->replicate();
            //$clone->id = Event::withTrashed()->max('id') + 1;
            $clone->name = "Copia di ".$clone->name;
            $clone->slug = $clone->slug."_".now()->timestamp;

            $clone->is_enabled = 0;
            $clone->save();

            foreach ($original->services as $service){
                $service_clone = $service->replicate();
                $service_clone->event_id = $clone->id;
                $service_clone->save();
                $relation[$service->id]=$service_clone->id;
            }

            if (!empty($original->bundles)){

                $bundles = (array)$original->bundles;

                foreach ($bundles as &$bundle){
                    foreach ($bundle as $key_bundle => &$value_bundle) {                    
                        if (!in_array($key_bundle, ['bundle_accomodations','bundle_events','bundle_services','bundle_extras'])){
                            continue;
                        }
                        $key_bundle = substr($key_bundle, 0, -1);

                        foreach ($value_bundle as &$bundle_service){
                            $bundle_service[$key_bundle] = $relation[$bundle_service[$key_bundle]];
                        }
                    }
                }
                $clone->bundles = $bundles;
            }


            if (!empty($original->upsells)){
                $upsells = (array)$original->upsells;
                foreach ($upsells as &$service){
                    $service['upsell_service'] = $relation[$service['upsell_service']];
                }
                $clone->upsells = $upsells;
            }

            
            if (!empty($original->event_gallery)) foreach ($original->event_gallery as $img){
                $copy = new \System\Models\File;
                $copy->fromUrl($img->getPath(), now()->timestamp.'.jpg');
                $clone->event_gallery()->add($copy);
            }

            $clone->save();
        }

        \Flash::success('Event cloned');
        return $this->listRefresh();
    }


}
