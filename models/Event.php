<?php namespace Quivi\Event\Models;

use Model;


use Quivi\Event\Models\Service;

/**
 * Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'name',
        ['slug', 'index' => true],
        'descr',
        'video_mp4',
        'video_webm',
    ];


    protected $jsonable = ['bundles', 'upsells', 'content', 'faqs'];

    protected $appends = ['base_price', 'default_bundle', 'days','father','has_video'];



    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_event_events';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    public $belongsTo = [
        'parent_event' => [
            'Quivi\Event\Models\Event',
            'key' => 'event_id',
            'otherKey' => 'id'
        ],
    ];

    public $hasMany = [


        'services' => [
            'Quivi\Event\Models\Service',
            'order'      => 'id asc',
        ]
    ];

    public $attachMany = [
        'event_gallery' => 'System\Models\File'
    ];


    public function getHeaderClassOptions() {
        return array('light' => 'Light theme', 'dark' => 'Dark Theme');
    }


    public function getAccomodationByType($type_slug = null) {

        return $this->products->filter(function($product) use ($type_slug) {

            return $product->type->slug == $type_slug;

        });
    }


    public function getBundleServiceOptions() {
        $options = [];
        foreach ($this->services as $service) {
            $options[$service->id] = $service->name;
        }
        return $options;
    }

    public function getBundleAccomodationOptions() {
        $options = [];
        foreach ($this->services as $service) {
            if ($service->product !== null &&  $service->product->type_id == 101) {
                $options[$service->id] = $service->name;
            }
        }
        return $options;
    }

    public function getBundleEventOptions() {
        $options = [];
        foreach ($this->services as $service) {
            if ($service->product->type_id == 102 || $service->product->type_id == 105) {
                $options[$service->id] = $service->name;
            }
        }
        return $options;
    }

    public function getBundleExtraOptions() {
        return $this->getBundleServiceOptions();
    }
    public function getUpsellServiceOptions() {
        return $this->getBundleServiceOptions();
    }

    public function getDaysAttribute() {
        return (strtotime($this->date_to) -  strtotime($this->date_from))/86400;
    }

    public function getBundlePrice($bundle, $custom = []) {

        $price = 0;
        $pax = !empty($custom['pax']) ? $custom['pax'] : 2;
        $date_to = !empty($custom['date_to']) ? $custom['date_to'] : $this->date_to;
        $date_from = !empty($custom['date_from']) ? $custom['date_from'] : $this->date_from;

        $days = (strtotime($date_to) -  strtotime($date_from))/86400;

        $services = array_merge((array)$bundle['bundle_accomodations'], (array)$bundle['bundle_services'], (array)$bundle['bundle_events']);

        foreach ($services as $bundle_service){

            if (!empty($bundle_service['bundle_accomodation'])){
                $service = Service::find($bundle_service['bundle_accomodation']);
            } elseif (!empty($bundle_service['bundle_service'])){
                $service = Service::find($bundle_service['bundle_service']);
            } elseif (!empty($bundle_service['bundle_event'])){
                $service = Service::find($bundle_service['bundle_event']);
            }

            $price+= $service->calculatePrice($days, $pax);
        }

        return $price/100;
        return 0;
    }

    public function getEventsAttribute() {

        return Event::where('event_id',$this->id)->where('is_enabled',1)->get();

    }

    public function getFatherAttribute() {

        if (!$this->isChild()) return null;
        return Event::where('id',$this->event_id)->first();

    }

    public function getRelatedEventsAttribute() {

        if (!$this->isChild()) return null;
        $children = Event::where('id',$this->event_id)->where('is_enabled',1)->first()->events;
        $current_id = $this->id;

        $children = $children->reject(function($event, $key) use($current_id) {

                return $event->id == $current_id;

            });

        return $children; // $children are related events

    }


    public function getBasePriceAttribute() {

        if ($this->isFather()) {
            $child_base_prices = [];
            foreach ($this->events as $child) {

                $child_base_prices[] = $child->getBundlePrice($child->default_bundle);
            }

            return (count($child_base_prices) > 0) ? min($child_base_prices) : 0;

        } else {
            return $this->getBundlePrice($this->default_bundle);
        }

        //var_dump($this->bundles); exit;
    }

    public function getDefaultBundleAttribute(){
        foreach ((array) $this->bundles as $bundle){
            return $bundle;
        }
    }

    public function getFullData($recursion_level = 0) {

        $event = $this->toArray();

        if (!isset($event["descr"]) && $this->father->descr)
            $event["descr"] = $this->father->descr; // Ereditala descrizione dal padre


        // BUNDLES
        $fieldsName = ["bundle_accomodation", "bundle_service", "bundle_extra", "bundle_event"];

        $services_ids = [];
        $upsell_services_ids = [];


        foreach(["collect","replace"] as $mode) {

            // BUNDLES
            foreach((array)$this->bundles as $index=>$bundle) {

                foreach((array)$fieldsName as $field) {

                    $services_collection = $field."s";

                    foreach($bundle[$services_collection] as $service_index=>$service) {

                        if ($mode == 'collect') {

                            if (!in_array($service[$field],$services_ids))
                                $services_ids[] = $service[$field];

                        } elseif($mode == 'replace') {

                            // I know... Horribili Visu
                            $event["bundles"][$index][$services_collection][$service_index][$field] = $services[$service[$field]];

                        }


                    }

                }

                // Price of the bundle

                $event["bundles"][$index]["base_price"] = $this->getBundlePrice($bundle);


            }


            //UPSELLS
            foreach((array)$this->upsells as $index=>$upsell) {

                if ($mode == 'collect') {

                    $upsell_services_ids[] = $upsell["upsell_service"];

                } elseif ($mode == 'replace') {

                    $event["upsells"][$index]["upsell_service"] = $upsell_services[$upsell["upsell_service"]];

                }

            }


            if ($mode == 'collect') {

                $services = [];
                foreach (Service::whereIn('id', $services_ids)->get() as $service) {
                    $services[$service->id] = $service->toFullDataArray();
                }

                // UPSELLS
                $upsell_services = [];
                foreach (Service::whereIn('id', $upsell_services_ids)->get() as $service_index=>$service) {
                    $upsell_services[$service->id] = $service->toFullDataArray();
                }



            }


        }



        $event["events"] = [];
        foreach($this->events as $ev) {
            $event["events"][] = $ev->getFullData();
        }
        //dd($event, $event["events"]);



        return $event;



    }

    /*
     * List columns
     */

    public function getEventNameInListAttribute()
    {

        if ($this->isChild()){
            return "<strong>".$this->father->name."</strong> / ".$this->name;
        }

        return "<strong>".$this->name."</strong>";
    }




    /*
     * Translations
     */
    public static function translateEventPageParams($params, $oldLocale, $newLocale) {
        $newParams = $params;

        $record = self::transWhere("slug", $params["slug"], $oldLocale)->first();

        if ($record) {
            $record->translateContext($newLocale);
            $newParams["slug-father"] = $record->parent_event->slug;
            $newParams["slug"] = $record->slug;
        }

        return $newParams;
    }

    public static function translateParentEventPageParams($params, $oldLocale, $newLocale) {
        $newParams = $params;

        $record = self::transWhere("slug", $params["slug"], $oldLocale)->first();

        if ($record) {
            $record->translateContext($newLocale);
            $newParams["slug"] = $record->slug;
        }

        return $newParams;
    }

    /*
     * Conditionals
     */

    public function isChild()
    {
        if (!$this->event_id || !is_numeric($this->event_id)) return false;
        if (Event::where('id','=',$this->event_id)->count() > 0) return true;
        else return false;
    }

    public function isFather()
    {
        if ($this->event_id && is_numeric($this->event_id)) return false;
        else return true;
    }

    public function getHasVideoAttribute()
    {
        return ($this->video_mp4 || $this->video_webm);
    }


}
