<?php namespace Quivi\Event\Models;

use Model;
use Config;

/**
 * Model
 */
class Service extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = [
        '@RainLab.Translate.Behaviors.TranslatableModel',
        'Initbiz.Money.Behaviors.MoneyFields'
    ];
    public $translatable = [
        'name',
        ['slug', 'index' => true],
        'descr',
    ];

    protected $jsonable = [
        'upgrades',
        'options'
    ];

    protected $appends = ['full_price','photo', 'photo_gallery'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'quivi_event_services';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'event' => 'Quivi\Event\Models\Event',
        'product' => 'Quivi\Product\Models\Product',
    ];


    public $moneyFields = [
        'service_price' => [
            'amountColumn' => 'price',
            'currencyIdColumn' => 'currency_id'
        ],
        'service_cost' => [
            'amountColumn' => 'cost',
            'currencyIdColumn' => 'currency_id'
        ]
    ];

    public function getServiceUpgradeOptions() {
        $options = [];
        foreach ($this->event->services as $service) {
            if ($service->id != $this->id){
                $options[$service->id] = $service->name;
            }
        }
        return $options;
    }

    public function getNameAttribute($value){
        if ($value) {
            return $value;
        } elseif ($this->product) {
            return $this->product->name;
        }
    }
    public function getDescrAttribute($value){
        if ($value) {
            return $value;
        } elseif ($this->product) {
            return $this->product->descr;
        }
    }

    public function getPrice(){
        if ($this->price) {
            return $this->price;
        } elseif ($this->product) {
            return $this->product->price;
        }
    }

    // Prezzo di tutti i prodotti non opzionali
    public function getFullPriceAttribute() {
        return $this->price;
    }

    public function getQuantity($pax){

        if ($this->pax){
            return ceil($pax / $this->pax);
        } else {
            return 1;
        }
    }

    public function calculatePrice($pax = NULL, $days = NULL){
        $price = $this->price;
        
        if ($days && ($this->is_daily || ($this->product !== null && $this->product->type_id == 101))) {
            $price *= $days;
        }

        $price *= $this->getQuantity($pax);
        return $price/$pax;
    }

    public function isAvailable($pax){
        return (!$this->stock || ($this->stock - $this->sold >= $this->getQuantity($pax)));
    }


    public function getPhotoAttribute() {

        return (isset($this->product) && $this->product->photo) ? url(Config::get('cms.storage.media.path').$this->product->photo) : null;

    }

    public function getPhotoGalleryAttribute() {
        $gallery = [];
        if  (isset($this->product) && $this->product->product_gallery && count($this->product->product_gallery) > 0) {
            foreach ($this->product->product_gallery as $media) {
                $gallery[] = $media->toArray();
            }
        }

        return $gallery;

    }

    // Function implementing an array with upgrades as Object and not as an ID
    public function toFullDataArray($recursion_level=0) {

        $services_ids = [];

        $service_array = $this->toArray();

        // Capire se questa modifica spacca qualcosa :-/
        if ($recursion_level == 0) {
            foreach(["collect","replace"] as $mode) {

                foreach ((array)$this->upgrades as $index => $upgrade) {


                    //$service_array["upgrades"][$index]["service_upgrade"] =
                    if ($mode == 'collect') {

                        $services_ids[] = $upgrade["service_upgrade"];

                    } elseif ($mode == 'replace') {

                        // Capire se
                        if (isset($service_array["upgrades"][$index]) && isset($services[$upgrade["service_upgrade"]]))
                            $service_array["upgrades"][$index]["service_upgrade"] = $services[$upgrade["service_upgrade"]];

                    }

                }


                if ($mode == 'collect') {

                    $services = [];

                    foreach (Service::whereIn('id', $services_ids)->get() as $service_index => $service) {
                        $services[$service->id] = $service->toFullDataArray(1);
                    }

                }
            }


        }
        //dd($services_ids);
        //dd($service_array);



        return $service_array;

    }


}
